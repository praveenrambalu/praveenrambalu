$(document).ready(function() {
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: 'animated', // default
        offset: 0, // default
        mobile: true, // default
        live: true,
        duration: 5 // default
    });
    wow.init();
});